extern crate nyx_space as nyx;

use nyx::md::ui::*;

fn main() -> Result<(), NyxError> {
    println!("Welcome to the Nyx tutorial!\nSelect branch 1-orbit-def to get started.");

    Ok(())
}
