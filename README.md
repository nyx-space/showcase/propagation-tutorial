# Propagation tutorial

This tutorial will introduce you to using Nyx in Rust. It is set up to work on Gitpod, which requires a Gitlab, Github or Bitbucket account to access.

Several topics are addressed in this tutorial, each on its own branch. Each branch builds on the previous one.

1. Orbit definition and manipulation
2. Orbit propagation and trajectory querying
3. Monte Carlo runs
4. High fidelity Monte Carlo analyses